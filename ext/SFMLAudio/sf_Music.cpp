#include "rb_common.h"


VALUE rb_cMusic = Qnil;

void rb_Music_Free(void* data)
{
  sf::Music* pdata = reinterpret_cast<sf::Music*>(data);
  if(pdata != nullptr) { delete pdata; }
}

VALUE rb_Music_Alloc(VALUE klass)
{
  sf::Music* data = new sf::Music();
  return Data_Wrap_Struct(klass, NULL, rb_Music_Free, data);
}

VALUE rb_MopenFromFile(VALUE self, VALUE filename) {
  Check_Type(filename, T_STRING);
  RB_GET_DATA(sf::Music, Qfalse);
  return data->openFromFile(StringValueCStr(filename)) ? Qtrue : Qfalse;
}

VALUE rb_MopenFromMemory(VALUE self, VALUE string) {
  Check_Type(string, T_STRING);
  RB_GET_DATA(sf::Music, Qfalse);
  rb_iv_set(self, "@memory", string);
  return data->openFromMemory(StringValuePtr(string), RSTRING_LEN(string)) ? Qtrue : Qfalse;
}

VALUE rb_MgetDuration(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2NUM(0));

  sf::Time time = data->getDuration();
  
  return DBL2NUM(static_cast<double>(time.asSeconds()));
}

VALUE rb_MgetLoopPoints(VALUE self) {
  RB_GET_DATA(sf::Music, Qnil);

  auto points = data->getLoopPoints();

  VALUE ary = rb_ary_new_capa(2);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(points.offset.asSeconds())));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(points.length.asSeconds())));

  return ary;
}

VALUE rb_MsetLoopPoints(VALUE self, VALUE offset, VALUE length) {
  RB_GET_DATA(sf::Music, Qnil);

  sf::Music::TimeSpan span = sf::Music::TimeSpan(sf::seconds(static_cast<float>(NUM2DBL(offset))), sf::seconds(static_cast<float>(NUM2DBL(length))));
  data->setLoopPoints(span);

  return Qnil;
}

VALUE rb_Mplay(VALUE self) {
  RB_GET_DATA(sf::Music, Qnil);

  data->play();

  return Qnil;
}

VALUE rb_Mpause(VALUE self) {
  RB_GET_DATA(sf::Music, Qnil);

  data->pause();

  return Qnil;
}

VALUE rb_Mstop(VALUE self) {
  RB_GET_DATA(sf::Music, Qnil);

  data->stop();

  return Qnil;
}

VALUE rb_Mplaying(VALUE self) {
  RB_GET_DATA(sf::Music, Qfalse);
  return data->getStatus() == sf::Music::Status::Playing ? Qtrue : Qfalse;
}

VALUE rb_Mpaused(VALUE self) {
  RB_GET_DATA(sf::Music, Qfalse);
  return data->getStatus() == sf::Music::Status::Paused ? Qtrue : Qfalse;
}

VALUE rb_Mstopped(VALUE self) {
  RB_GET_DATA(sf::Music, Qfalse);
  return data->getStatus() == sf::Music::Status::Stopped ? Qtrue : Qfalse;
}

VALUE rb_MgetChannelCount(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2NUM(0));
  
  return UINT2NUM(data->getChannelCount());
}

VALUE rb_MgetSampleRate(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2NUM(0));
  
  return UINT2NUM(data->getSampleRate());
}

VALUE rb_MsetPlayingOffset(VALUE self, VALUE offset) {
  RB_GET_DATA(sf::Music, Qnil);
  
  data->setPlayingOffset(sf::seconds(static_cast<float>(NUM2DBL(offset))));

  return Qnil;
}

VALUE rb_MgetPlayingOffset(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2NUM(0));

  return DBL2NUM(static_cast<double>(data->getPlayingOffset().asSeconds()));
}

VALUE rb_MsetLoop(VALUE self, VALUE looping) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setLoop(RTEST(looping));

  return Qnil;
}

VALUE rb_MgetLoop(VALUE self) {
  RB_GET_DATA(sf::Music, Qfalse);

  return data->getLoop() ? Qtrue : Qfalse;
}

VALUE rb_MsetPitch(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setPitch(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_MgetPitch(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getPitch()));
}

VALUE rb_MsetVolume(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setVolume(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_MgetVolume(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getVolume()));
}

VALUE rb_MsetPosition(VALUE self, VALUE x, VALUE y, VALUE z) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setPosition(static_cast<float>(NUM2DBL(x)), static_cast<float>(NUM2DBL(y)), static_cast<float>(NUM2DBL(z)));

  return Qnil;
}

VALUE rb_MgetPosition(VALUE self) {
  RB_GET_DATA(sf::Music, Qnil);

  auto pos = data->getPosition();

  VALUE ary = rb_ary_new_capa(3);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.z)));

  return ary;
}

VALUE rb_MsetRelativeToListener(VALUE self, VALUE rel) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setLoop(RTEST(rel));

  return Qnil;
}

VALUE rb_MgetRelativeToListener(VALUE self) {
  RB_GET_DATA(sf::Music, Qfalse);

  return data->isRelativeToListener() ? Qtrue : Qfalse;
}

VALUE rb_MsetMinDistance(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setMinDistance(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_MgetMinDistance(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getMinDistance()));
}

VALUE rb_MsetAttenuation(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Music, Qnil);

  data->setAttenuation(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_MgetAttenuation(VALUE self) {
  RB_GET_DATA(sf::Music, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getAttenuation()));
}

void Init_Music(VALUE mainModule) {
  rb_cMusic = rb_define_class_under(mainModule, "Music", rb_cSoundStream);
  rb_define_alloc_func(rb_cMusic, rb_Music_Alloc);

  rb_define_method(rb_cMusic, "open_from_file", _rbf rb_MopenFromFile, 1);
  rb_define_method(rb_cMusic, "open_from_memory", _rbf rb_MopenFromMemory, 1);
  rb_define_method(rb_cMusic, "get_duration", _rbf rb_MgetDuration, 0);
  rb_define_method(rb_cMusic, "get_loop_points", _rbf rb_MgetLoopPoints, 0);
  rb_define_method(rb_cMusic, "set_loop_points", _rbf rb_MsetLoopPoints, 2);
  rb_define_method(rb_cMusic, "play", _rbf rb_Mplay, 0);
  rb_define_method(rb_cMusic, "pause", _rbf rb_Mpause, 0);
  rb_define_method(rb_cMusic, "stop", _rbf rb_Mstop, 0);
  rb_define_method(rb_cMusic, "playing?", _rbf rb_Mplaying, 0);
  rb_define_method(rb_cMusic, "paused?", _rbf rb_Mpaused, 0);
  rb_define_method(rb_cMusic, "stopped?", _rbf rb_Mstopped, 0);
  rb_define_method(rb_cMusic, "get_channel_count", _rbf rb_MgetChannelCount, 0);
  rb_define_method(rb_cMusic, "get_sample_rate", _rbf rb_MgetSampleRate, 0);
  rb_define_method(rb_cMusic, "set_playing_offset", _rbf rb_MsetPlayingOffset, 1);
  rb_define_method(rb_cMusic, "get_playing_offset", _rbf rb_MgetPlayingOffset, 0);
  rb_define_method(rb_cMusic, "set_loop", _rbf rb_MsetLoop, 1);
  rb_define_method(rb_cMusic, "get_loop", _rbf rb_MgetLoop, 0);
  rb_define_method(rb_cMusic, "set_pitch", _rbf rb_MsetPitch, 1);
  rb_define_method(rb_cMusic, "get_pitch", _rbf rb_MgetPitch, 0);
  rb_define_method(rb_cMusic, "set_volume", _rbf rb_MsetVolume, 1);
  rb_define_method(rb_cMusic, "get_volume", _rbf rb_MgetVolume, 0);
  rb_define_method(rb_cMusic, "set_position", _rbf rb_MsetPosition, 3);
  rb_define_method(rb_cMusic, "get_position", _rbf rb_MgetPosition, 0);
  rb_define_method(rb_cMusic, "set_relative_to_listener", _rbf rb_MsetRelativeToListener, 1);
  rb_define_method(rb_cMusic, "relative_to_listener?", _rbf rb_MgetRelativeToListener, 0);
  rb_define_method(rb_cMusic, "set_min_distance", _rbf rb_MsetMinDistance, 1);
  rb_define_method(rb_cMusic, "get_min_distance", _rbf rb_MgetMinDistance, 0);
  rb_define_method(rb_cMusic, "set_attenuation", _rbf rb_MsetAttenuation, 1);
  rb_define_method(rb_cMusic, "get_attenuation", _rbf rb_MgetAttenuation, 0);
}
