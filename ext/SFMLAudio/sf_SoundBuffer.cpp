#include "rb_common.h"


VALUE rb_cSoundBuffer = Qnil;

void rb_SoundBuffer_Free(void* data) {
  sf::SoundBuffer* pdata = reinterpret_cast<sf::SoundBuffer*>(data);
  if(pdata != nullptr) { delete pdata; }
}

VALUE rb_SoundBuffer_Alloc(VALUE klass) {
  sf::SoundBuffer* data = new sf::SoundBuffer();
  return Data_Wrap_Struct(klass, NULL, rb_SoundBuffer_Free, data);
}

VALUE rb_SBloadFromFile(VALUE self, VALUE filename) {
  Check_Type(filename, T_STRING);
  RB_GET_DATA(sf::SoundBuffer, Qfalse);
  return data->loadFromFile(StringValueCStr(filename)) ? Qtrue : Qfalse;
}

VALUE rb_SBloadFromMemory(VALUE self, VALUE string) {
  Check_Type(string, T_STRING);
  RB_GET_DATA(sf::SoundBuffer, Qfalse);
  rb_iv_set(self, "@memory", string);
  return data->loadFromMemory(StringValuePtr(string), RSTRING_LEN(string)) ? Qtrue : Qfalse;
}

VALUE rb_SBgetSampleCount(VALUE self) {
  RB_GET_DATA(sf::SoundBuffer, LONG2NUM(0));
  
  return ULL2NUM(data->getSampleCount());
}

VALUE rb_SBgetChannelCount(VALUE self) {
  RB_GET_DATA(sf::SoundBuffer, LONG2NUM(0));
  
  return UINT2NUM(data->getChannelCount());
}

VALUE rb_SBgetSampleRate(VALUE self) {
  RB_GET_DATA(sf::SoundBuffer, LONG2NUM(0));
  
  return UINT2NUM(data->getSampleRate());
}

VALUE rb_SBgetDuration(VALUE self) {
  RB_GET_DATA(sf::SoundBuffer, LONG2NUM(0));

  sf::Time time = data->getDuration();
  
  return DBL2NUM(static_cast<double>(time.asSeconds()));
}

sf::SoundBuffer* rb_SoundBufferGet(VALUE self) {
  if (rb_obj_is_kind_of(self, rb_cSoundBuffer) != Qtrue) {
    return nullptr;
  }
  RB_GET_DATA(sf::SoundBuffer, nullptr);
  return data;
}


void InitSoundBuffer(VALUE mainModule) {
  rb_cSoundBuffer = rb_define_class_under(mainModule, "SoundBuffer", rb_cObject);
  rb_define_alloc_func(rb_cSoundBuffer, rb_SoundBuffer_Alloc);

  rb_define_method(rb_cSoundBuffer, "load_from_file", _rbf rb_SBloadFromFile, 1);
  rb_define_method(rb_cSoundBuffer, "load_from_memory", _rbf rb_SBloadFromMemory, 1);
  rb_define_method(rb_cSoundBuffer, "get_sample_count", _rbf rb_SBgetSampleCount, 0);
  rb_define_method(rb_cSoundBuffer, "get_channel_count", _rbf rb_SBgetChannelCount, 0);
  rb_define_method(rb_cSoundBuffer, "get_sample_rate", _rbf rb_SBgetSampleRate, 0);
  rb_define_method(rb_cSoundBuffer, "get_duration", _rbf rb_SBgetDuration, 0);
}
