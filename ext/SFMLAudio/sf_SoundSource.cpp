#include "rb_common.h"

VALUE rb_cSoundSource = Qnil;

void Init_SoundSource(VALUE mainModule) {
  rb_cSoundSource = rb_define_class_under(mainModule, "SoundSource", rb_cObject);
}
