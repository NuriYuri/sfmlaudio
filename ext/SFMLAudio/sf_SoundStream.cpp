#include "rb_common.h"

VALUE rb_cSoundStream = Qnil;

void Init_SoundStream(VALUE mainModule) {
  rb_cSoundStream = rb_define_class_under(mainModule, "SoundStream", rb_cSoundSource);
}
