#ifndef L_UTILS_RUBY_COMMON_HEADER
#define L_UTILS_RUBY_COMMON_HEADER

#include "ruby.h"
#include "SFML/Audio.hpp"
#define _rbf (VALUE (*)(...))

#define RB_PROTECT_SELF(ret) if(RDATA(self)->data == nullptr) \
    return ret; \

#define RB_GET_DATA(type, ret) \
  type* data; \
  RB_PROTECT_SELF(ret) \
  Data_Get_Struct(self, type, data);

void InitInputSoundFile(VALUE mainModule);
extern VALUE rb_cInputSoundFile;
void Init_Listener(VALUE mainModule);
extern VALUE rb_mListener;
void Init_SoundSource(VALUE mainModule);
extern VALUE rb_cSoundSource;
void Init_SoundStream(VALUE mainModule);
extern VALUE rb_cSoundStream;
void Init_Music(VALUE mainModule);
extern VALUE rb_cMusic;
void Init_Sound(VALUE mainModule);
extern VALUE rb_cSound;
void InitSoundBuffer(VALUE mainModule);
extern VALUE rb_cSoundBuffer;

sf::SoundBuffer* rb_SoundBufferGet(VALUE self);
#endif
