#include "rb_common.h"


VALUE rb_cInputSoundFile = Qnil;

void rb_InputSoundFile_Free(void* data) {
  sf::InputSoundFile* pdata = reinterpret_cast<sf::InputSoundFile*>(data);
  if(pdata != nullptr) { delete pdata; }
}

VALUE rb_InputSoundFile_Alloc(VALUE klass) {
  sf::InputSoundFile* data = new sf::InputSoundFile();
  return Data_Wrap_Struct(klass, NULL, rb_InputSoundFile_Free, data);
}

VALUE rb_ISFopenFromFile(VALUE self, VALUE filename) {
  Check_Type(filename, T_STRING);
  RB_GET_DATA(sf::InputSoundFile, Qfalse);
  return data->openFromFile(StringValueCStr(filename)) ? Qtrue : Qfalse;
}

VALUE rb_ISFopenFromMemory(VALUE self, VALUE string) {
  Check_Type(string, T_STRING);
  RB_GET_DATA(sf::InputSoundFile, Qfalse);
  rb_iv_set(self, "@memory", string);
  return data->openFromMemory(StringValuePtr(string), RSTRING_LEN(string)) ? Qtrue : Qfalse;
}

VALUE rb_ISFgetSampleCount(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));
  
  return ULL2NUM(data->getSampleCount());
}

VALUE rb_ISFgetChannelCount(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));
  
  return UINT2NUM(data->getChannelCount());
}

VALUE rb_ISFgetSampleRate(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));
  
  return UINT2NUM(data->getSampleRate());
}

VALUE rb_ISFgetDuration(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));

  sf::Time time = data->getDuration();
  
  return DBL2NUM(static_cast<double>(time.asSeconds()));
}

VALUE rb_ISFgetTimeOffset(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));

  sf::Time time = data->getTimeOffset();
  
  return DBL2NUM(static_cast<double>(time.asSeconds()));
}

VALUE rb_ISFgetSampleOffset(VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, LONG2NUM(0));
  
  return ULL2NUM(data->getSampleOffset());
}

VALUE rb_ISFseek(int argc, VALUE* argv, VALUE self) {
  RB_GET_DATA(sf::InputSoundFile, Qnil);

  VALUE numberOfSamples, asSecond;
  rb_scan_args(argc, argv, "11", &numberOfSamples, &asSecond);

  if (asSecond) {
    data->seek(sf::seconds(static_cast<float>(NUM2DBL(numberOfSamples))));
  } else {
    data->seek(static_cast<sf::Uint64>(NUM2ULL(numberOfSamples)));
  }

  return Qnil;
}

VALUE rb_ISFread(VALUE self, VALUE numberOfSamples) {
  RB_GET_DATA(sf::InputSoundFile, rb_ary_new());
  
  sf::Uint64 count = static_cast<sf::Uint64>(NUM2ULL(numberOfSamples));
  sf::Int16* samples = new sf::Int16[count];
  sf::Uint64 result = data->read(samples, count);

  VALUE ary = rb_ary_new_capa(static_cast<long>(result));

  for (unsigned long long i = 0; i < result; i++) {
    rb_ary_push(ary, LONG2FIX(samples[i]));
  }

  delete[] samples;

  return ary;
}

void InitInputSoundFile(VALUE mainModule) {
  rb_cInputSoundFile = rb_define_class_under(mainModule, "InputSoundFile", rb_cObject);
  rb_define_alloc_func(rb_cInputSoundFile, rb_InputSoundFile_Alloc);

  rb_define_method(rb_cInputSoundFile, "open_from_file", _rbf rb_ISFopenFromFile, 1);
  rb_define_method(rb_cInputSoundFile, "open_from_memory", _rbf rb_ISFopenFromMemory, 1);
  rb_define_method(rb_cInputSoundFile, "get_sample_count", _rbf rb_ISFgetSampleCount, 0);
  rb_define_method(rb_cInputSoundFile, "get_channel_count", _rbf rb_ISFgetChannelCount, 0);
  rb_define_method(rb_cInputSoundFile, "get_sample_rate", _rbf rb_ISFgetSampleRate, 0);
  rb_define_method(rb_cInputSoundFile, "get_duration", _rbf rb_ISFgetDuration, 0);
  rb_define_method(rb_cInputSoundFile, "get_time_offset", _rbf rb_ISFgetTimeOffset, 0);
  rb_define_method(rb_cInputSoundFile, "get_sample_offset", _rbf rb_ISFgetSampleOffset, 0);
  rb_define_method(rb_cInputSoundFile, "seek", _rbf rb_ISFseek, -1);
  rb_define_method(rb_cInputSoundFile, "read", _rbf rb_ISFread, 1);
}
