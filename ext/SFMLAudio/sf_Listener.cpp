#include "rb_common.h"

VALUE rb_mListener = Qnil;

VALUE rb_LsetGlobalVolume(VALUE self, VALUE volume) {
  sf::Listener::setGlobalVolume(static_cast<float>(NUM2DBL(volume)));
  return Qnil;
}

VALUE rb_LgetGlobalVolume(VALUE self) {
  return DBL2NUM(static_cast<double>(sf::Listener::getGlobalVolume()));
}

VALUE rb_LsetPosition(int argc, VALUE* argv, VALUE self) {
  VALUE x, y, z;
  rb_scan_args(argc, argv, "12", &x, &y, &z);
  if (argc == 1) {
    Check_Type(x, T_ARRAY);
    y = rb_ary_entry(x, 1);
    z = rb_ary_entry(x, 2);
    x = rb_ary_entry(x, 0);
  }
  sf::Listener::setPosition(static_cast<float>(NUM2DBL(x)), static_cast<float>(NUM2DBL(y)), static_cast<float>(NUM2DBL(z)));
  return Qnil;
}

VALUE rb_LgetPosition(VALUE self) {
  sf::Vector3f pos = sf::Listener::getPosition();

  VALUE ary = rb_ary_new_capa(3);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.z)));

  return ary;
}

VALUE rb_LsetDirection(int argc, VALUE* argv, VALUE self) {
  VALUE x, y, z;
  rb_scan_args(argc, argv, "12", &x, &y, &z);
  if (argc == 1) {
    Check_Type(x, T_ARRAY);
    y = rb_ary_entry(x, 1);
    z = rb_ary_entry(x, 2);
    x = rb_ary_entry(x, 0);
  }
  sf::Listener::setDirection(static_cast<float>(NUM2DBL(x)), static_cast<float>(NUM2DBL(y)), static_cast<float>(NUM2DBL(z)));
  return Qnil;
}

VALUE rb_LgetDirection(VALUE self) {
  sf::Vector3f pos = sf::Listener::getDirection();

  VALUE ary = rb_ary_new_capa(3);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.z)));

  return ary;
}

VALUE rb_LsetUpVector(int argc, VALUE* argv, VALUE self) {
  VALUE x, y, z;
  rb_scan_args(argc, argv, "12", &x, &y, &z);
  if (argc == 1) {
    Check_Type(x, T_ARRAY);
    y = rb_ary_entry(x, 1);
    z = rb_ary_entry(x, 2);
    x = rb_ary_entry(x, 0);
  }
  sf::Listener::setUpVector(static_cast<float>(NUM2DBL(x)), static_cast<float>(NUM2DBL(y)), static_cast<float>(NUM2DBL(z)));
  return Qnil;
}

VALUE rb_LgetUpVector(VALUE self) {
  sf::Vector3f pos = sf::Listener::getUpVector();

  VALUE ary = rb_ary_new_capa(3);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.z)));

  return ary;
}

void Init_Listener(VALUE mainModule) {
  rb_mListener = rb_define_module_under(mainModule, "Listener");

  rb_define_module_function(rb_mListener, "set_globale_volume", _rbf rb_LsetGlobalVolume, 1);
  rb_define_module_function(rb_mListener, "get_globale_volume", _rbf rb_LgetGlobalVolume, 0);
  rb_define_module_function(rb_mListener, "set_position", _rbf rb_LsetPosition, -1);
  rb_define_module_function(rb_mListener, "get_position", _rbf rb_LgetPosition, 0);
  rb_define_module_function(rb_mListener, "set_direction", _rbf rb_LsetDirection, -1);
  rb_define_module_function(rb_mListener, "get_direction", _rbf rb_LgetDirection, 0);
  rb_define_module_function(rb_mListener, "set_up_vector", _rbf rb_LsetUpVector, -1);
  rb_define_module_function(rb_mListener, "get_up_vector", _rbf rb_LgetUpVector, 0);
}
