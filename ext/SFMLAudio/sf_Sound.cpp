#include "rb_common.h"


VALUE rb_cSound = Qnil;

void rb_Sound_Free(void* data)
{
  sf::Sound* pdata = reinterpret_cast<sf::Sound*>(data);
  if(pdata != nullptr) { delete pdata; }
}

VALUE rb_Sound_Alloc(VALUE klass)
{
  sf::Sound* data = new sf::Sound();
  return Data_Wrap_Struct(klass, NULL, rb_Sound_Free, data);
}

VALUE rb_Splay(VALUE self) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->play();

  return Qnil;
}

VALUE rb_Spause(VALUE self) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->pause();

  return Qnil;
}

VALUE rb_Sstop(VALUE self) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->stop();

  return Qnil;
}

VALUE rb_Splaying(VALUE self) {
  RB_GET_DATA(sf::Sound, Qfalse);
  return data->getStatus() == sf::Sound::Status::Playing ? Qtrue : Qfalse;
}

VALUE rb_Spaused(VALUE self) {
  RB_GET_DATA(sf::Sound, Qfalse);
  return data->getStatus() == sf::Sound::Status::Paused ? Qtrue : Qfalse;
}

VALUE rb_Sstopped(VALUE self) {
  RB_GET_DATA(sf::Sound, Qfalse);
  return data->getStatus() == sf::Sound::Status::Stopped ? Qtrue : Qfalse;
}

VALUE rb_SsetPlayingOffset(VALUE self, VALUE offset) {
  RB_GET_DATA(sf::Sound, Qnil);
  
  data->setPlayingOffset(sf::seconds(static_cast<float>(NUM2DBL(offset))));

  return Qnil;
}

VALUE rb_SgetPlayingOffset(VALUE self) {
  RB_GET_DATA(sf::Sound, LONG2NUM(0));

  return DBL2NUM(static_cast<double>(data->getPlayingOffset().asSeconds()));
}

VALUE rb_SsetLoop(VALUE self, VALUE looping) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setLoop(RTEST(looping));

  return Qnil;
}

VALUE rb_SgetLoop(VALUE self) {
  RB_GET_DATA(sf::Sound, Qfalse);

  return data->getLoop() ? Qtrue : Qfalse;
}

VALUE rb_SsetPitch(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setPitch(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_SgetPitch(VALUE self) {
  RB_GET_DATA(sf::Sound, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getPitch()));
}

VALUE rb_SsetVolume(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setVolume(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_SgetVolume(VALUE self) {
  RB_GET_DATA(sf::Sound, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getVolume()));
}

VALUE rb_SsetPosition(VALUE self, VALUE x, VALUE y, VALUE z) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setPosition(static_cast<float>(NUM2DBL(x)), static_cast<float>(NUM2DBL(y)), static_cast<float>(NUM2DBL(z)));

  return Qnil;
}

VALUE rb_SgetPosition(VALUE self) {
  RB_GET_DATA(sf::Sound, Qnil);

  auto pos = data->getPosition();

  VALUE ary = rb_ary_new_capa(3);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.z)));

  return ary;
}

VALUE rb_SsetRelativeToListener(VALUE self, VALUE rel) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setLoop(RTEST(rel));

  return Qnil;
}

VALUE rb_SgetRelativeToListener(VALUE self) {
  RB_GET_DATA(sf::Sound, Qfalse);

  return data->isRelativeToListener() ? Qtrue : Qfalse;
}

VALUE rb_SsetMinDistance(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setMinDistance(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_SgetMinDistance(VALUE self) {
  RB_GET_DATA(sf::Sound, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getMinDistance()));
}

VALUE rb_SsetAttenuation(VALUE self, VALUE pitch) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->setAttenuation(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_SgetAttenuation(VALUE self) {
  RB_GET_DATA(sf::Sound, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getAttenuation()));
}

VALUE rb_SsetBuffer(VALUE self, VALUE buffer) {
  RB_GET_DATA(sf::Sound, Qnil);

  auto soundBuffer = rb_SoundBufferGet(buffer);
  if (soundBuffer == nullptr) {
    return Qnil;
  }

  rb_iv_set(self, "@buffer", buffer);
  data->setBuffer(*soundBuffer);

  return Qnil;
}

VALUE rb_SgetBuffer(VALUE self) {
  return rb_iv_get(self, "@buffer");
}

VALUE rb_SresetBuffer(VALUE self) {
  RB_GET_DATA(sf::Sound, Qnil);

  data->resetBuffer();

  return Qnil;
}

void Init_Sound(VALUE mainModule) {
  rb_cSound = rb_define_class_under(mainModule, "Sound", rb_cSoundSource);
  rb_define_alloc_func(rb_cSound, rb_Sound_Alloc);

  rb_define_method(rb_cSound, "play", _rbf rb_Splay, 0);
  rb_define_method(rb_cSound, "pause", _rbf rb_Spause, 0);
  rb_define_method(rb_cSound, "stop", _rbf rb_Sstop, 0);
  rb_define_method(rb_cSound, "playing?", _rbf rb_Splaying, 0);
  rb_define_method(rb_cSound, "paused?", _rbf rb_Spaused, 0);
  rb_define_method(rb_cSound, "stopped?", _rbf rb_Sstopped, 0);
  rb_define_method(rb_cSound, "set_playing_offset", _rbf rb_SsetPlayingOffset, 1);
  rb_define_method(rb_cSound, "get_playing_offset", _rbf rb_SgetPlayingOffset, 0);
  rb_define_method(rb_cSound, "set_loop", _rbf rb_SsetLoop, 1);
  rb_define_method(rb_cSound, "get_loop", _rbf rb_SgetLoop, 0);
  rb_define_method(rb_cSound, "set_pitch", _rbf rb_SsetPitch, 1);
  rb_define_method(rb_cSound, "get_pitch", _rbf rb_SgetPitch, 0);
  rb_define_method(rb_cSound, "set_volume", _rbf rb_SsetVolume, 1);
  rb_define_method(rb_cSound, "get_volume", _rbf rb_SgetVolume, 0);
  rb_define_method(rb_cSound, "set_position", _rbf rb_SsetPosition, 3);
  rb_define_method(rb_cSound, "get_position", _rbf rb_SgetPosition, 0);
  rb_define_method(rb_cSound, "set_relative_to_listener", _rbf rb_SsetRelativeToListener, 1);
  rb_define_method(rb_cSound, "relative_to_listener?", _rbf rb_SgetRelativeToListener, 0);
  rb_define_method(rb_cSound, "set_min_distance", _rbf rb_SsetMinDistance, 1);
  rb_define_method(rb_cSound, "get_min_distance", _rbf rb_SgetMinDistance, 0);
  rb_define_method(rb_cSound, "set_attenuation", _rbf rb_SsetAttenuation, 1);
  rb_define_method(rb_cSound, "get_attenuation", _rbf rb_SgetAttenuation, 0);
  rb_define_method(rb_cSound, "set_buffer", _rbf rb_SsetBuffer, 1);
  rb_define_method(rb_cSound, "get_buffer", _rbf rb_SgetBuffer, 0);
  rb_define_method(rb_cSound, "reset_buffer", _rbf rb_SresetBuffer, 0);
}
