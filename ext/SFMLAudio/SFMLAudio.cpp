#include "rb_common.h"

VALUE rb_mSFMLAudio = Qnil;

extern "C" {
  void Init_SFMLAudio() {
    rb_mSFMLAudio = rb_define_module("SFMLAudio");

    InitInputSoundFile(rb_mSFMLAudio);
    Init_Listener(rb_mSFMLAudio);
    Init_SoundSource(rb_mSFMLAudio);
    Init_SoundStream(rb_mSFMLAudio);
    Init_Music(rb_mSFMLAudio);
    Init_Sound(rb_mSFMLAudio);
    InitSoundBuffer(rb_mSFMLAudio);
  }
}
