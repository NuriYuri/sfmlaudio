# frozen_string_literal: true

require 'mkmf'

sfml_dir_env = ENV['SFML_DIR']
if sfml_dir_env
  $INCFLAGS << " -I'" + (sfml_dir_env + "/include'")
  $LDFLAGS << " -L'" + (sfml_dir_env + "/lib'")
end

ext_name = 'SFMLAudio'

have_library('sfml-audio')
have_library('sfml-system')

ASSEMBLE_CXX << ' -std=c++14'
COMPILE_CXX << ' -std=c++14'

create_makefile(ext_name)
